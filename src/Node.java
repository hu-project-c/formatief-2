public class Node {
    private final int state;
    private Node A;
    private Node B;

    public Node(int state){
        this.state = state;
    }

    public int getState(){
        return this.state;
    }

    public void setA(Node a) {
        this.A = a;
    }

    public void setB(Node b) {
        this.B = b;
    }

    public Node getA(){
        return this.A;
    }

    public Node getB(){
        return this.B;
    }
}
