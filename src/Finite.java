import java.util.LinkedList;
import java.util.List;

public class Finite {

    private final String inputString = "BAABA";
    Node node0 = new Node(0);
    Node node1 = new Node(1);
    Node node2 = new Node(2);
    Node node3 = new Node(3);

    public Finite(){

        this.node0.setA(node2);
        this.node0.setB(node1);

        this.node1.setA(node1);
        this.node1.setB(node2);

        this.node2.setB(node3);
        this.node3.setA(node3);

        this.node3.setB(node0);

       LinkedList<Integer> path = this.readString();

       System.out.println(path);
    }

    public LinkedList<Integer> readString(){
        LinkedList<Integer> pathList = new LinkedList<>();

        Node currentNode = this.node0;
        pathList.add(currentNode.getState());
        for (int i = 0; i < this.inputString.length(); i++){
            char c = this.inputString.charAt(i);
            if (c == 'A'){
                if (currentNode.getState() == 2){
                    currentNode = this.node3;
                } else {
                    currentNode = currentNode.getA();
                }
                pathList.add(currentNode.getState());
            } else if (c == 'B') {
                currentNode = currentNode.getB();
                pathList.add(currentNode.getState());
            }
        }

        return pathList;
    }

}
